package Prakt_3B;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
public class latihan_1AB extends JFrame {
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_hEIGHT = 200 ;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField txtField;
    public static void main(String[] args) {
        latihan_1AB frame = new latihan_1AB();
        frame.setVisible(true);
    }
    public latihan_1AB(){
        Container contentPane = getContentPane();
        
        setSize(FRAME_WIDTH,FRAME_hEIGHT);
        setResizable (true);
        setTitle ("Program Ch13AbsolutePositioning");
        setLocation(FRAME_X_ORIGIN,FRAME_Y_ORIGIN);
        
        contentPane.setLayout(null);
        contentPane.setBackground(Color.white);
        
        okButton = new JButton("OK");
        okButton.setBounds(50,100,BUTTON_WIDTH,BUTTON_HEIGHT);
        contentPane.add(okButton);
        
        cancelButton = new JButton("CANCEL");
        cancelButton.setBounds(200,100,BUTTON_WIDTH,BUTTON_HEIGHT);
        contentPane.add(cancelButton);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
}
}
