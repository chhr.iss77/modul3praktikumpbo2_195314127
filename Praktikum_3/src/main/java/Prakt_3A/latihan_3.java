package Prakt_3A;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
public class latihan_3 extends JFrame{
    public latihan_3(){
        this.setSize(300,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);
        
        JPanel panel=new JPanel();
        JButton tombol = new JButton();
        tombol.setText("ini Tombol");
        panel.add(tombol);
        this.add(panel);
        
        JLabel label=new JLabel("labelku");
        panel.add(label);
        this.add(panel);
        
        JTextField text= new JTextField();
        panel.add(text);
        this.add(panel);
        
        JCheckBox box=new JCheckBox("checkbox");
        panel.add(box);
        this.add(panel);
        
        JRadioButton radio = new JRadioButton("Radio Button");
        panel.add(radio);
        this.add(panel);
    }
    public static void main(String[] args) {
        new latihan_3();
    }
}
